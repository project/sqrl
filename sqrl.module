<?php

/**
 * @file
 * Module file of the SQRL module.
 */

use Drupal\Component\Render\PlainTextOutput;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user\UserInterface;

/**
 * Implements hook_mail_alter().
 */
function sqrl_mail_alter(array &$message): void {
  // Avoid sending emails to SQRL accounts that do not have an email address or
  // just one of those default ones, which aren't valid anyway.
  if (empty($message['to']) || Drupal::service('sqrl.identities')->isDummyMail($message['to'])) {
    $message['send'] = FALSE;
    Drupal::messenger()->addWarning('Email not sent as your account does not have a valid email address associated with it.');
  }
}

/**
 * Implements hook_user_cancel().
 */
function sqrl_user_cancel(array $edit, UserInterface $account): void {
  Drupal::service('sqrl.identities')->cancel($account);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function sqrl_form_user_login_form_alter(array &$form): void {
  Drupal::service('sqrl.form_alter')->loginForm($form);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function sqrl_form_user_login_block_alter(array &$form): void {
  Drupal::service('sqrl.form_alter')->loginForm($form);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function sqrl_form_user_register_form_alter(array &$form): void {
  Drupal::service('sqrl.form_alter')->registerForm($form);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function sqrl_form_user_pass_alter(array &$form): void {
  Drupal::service('sqrl.form_alter')->passwordForm($form);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function sqrl_form_user_form_alter(array &$form, FormStateInterface $form_state): void {
  Drupal::service('sqrl.form_alter')->userForm($form, $form_state);
}

/**
 * Implements hook_theme().
 */
function sqrl_theme(): array {
  return [
    'sqrl_widget' => [
      'variables' => [
        'title' => NULL,
        'description' => NULL,
        'operation' => NULL,
        'nuturl' => NULL,
        'encodednuturl' => NULL,
        'qrcodeurl' => NULL,
        'qrcodesize' => NULL,
        'logourl' => NULL,
      ],
    ],
    'sqrl_widget_cached' => [
      'variables' => [
        'title' => NULL,
        'description' => NULL,
        'operation' => NULL,
        'url' => NULL,
        'logourl' => NULL,
      ],
    ],
  ];
}

/**
 * Implements hook_mail().
 */
function sqrl_mail(string $key, array &$message, array $params): void {
  if ($key === 'confirm_email') {
    $token_service = Drupal::token();
    $language_manager = Drupal::languageManager();
    $langcode = $message['langcode'];
    $variables = ['user' => $params['account']];

    $language = $language_manager->getLanguage($params['account']->getPreferredLangcode());
    $language_manager->setConfigOverrideLanguage($language);

    $token_options = ['langcode' => $langcode, 'clear' => TRUE];

    $subject = 'Email address confirmation for [user:display-name] at [site:name]';
    $message['subject'] .= PlainTextOutput::renderFromHtml($token_service->replace($subject, $variables, $token_options));
    $message['body'][] = t('Please confirm your email address by clicking <a href="@url">this link</a> or paste the following URL into your browser: @url', [
      '@url' => Url::fromRoute('sqrl.profile.confirm', [
        'user' => $params['account']->id(),
        'token' => $params['token'],
      ], ['absolute' => TRUE])->toString(),
    ]);
  }
}
