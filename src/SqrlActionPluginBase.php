<?php

namespace Drupal\sqrl;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\Random;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for sqrl_action plugins.
 */
abstract class SqrlActionPluginBase extends PluginBase implements SqrlActionInterface {

  use StringTranslationTrait;

  /**
   * The sqrl service.
   *
   * @var \Drupal\sqrl\Sqrl
   */
  protected Sqrl $sqrl;

  /**
   * The client service.
   *
   * @var \Drupal\sqrl\Client
   */
  protected Client $client;

  /**
   * The state.
   *
   * @var \Drupal\sqrl\State
   */
  protected State $state;

  /**
   * The log channel.
   *
   * @var \Drupal\sqrl\Log
   */
  protected Log $log;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected ConfigFactory $configFactory;

  /**
   * The random service.
   *
   * @var \Drupal\Component\Utility\Random
   */
  protected Random $random;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * The identities service.
   *
   * @var \Drupal\sqrl\Identities
   */
  protected Identities $identities;

  /**
   * The user account.
   *
   * @var \Drupal\user\UserInterface|null
   */
  protected ?UserInterface $account;

  /**
   * {@inheritdoc}
   */
  final public function __construct(array $configuration, $plugin_id, $plugin_definition, Sqrl $sqrl, Client $client, State $state, Log $log, ConfigFactory $config_factory, TimeInterface $time, Identities $identities) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->sqrl = $sqrl;
    $this->client = $client;
    $this->state = $state;
    $this->log = $log;
    $this->configFactory = $config_factory;
    $this->time = $time;
    $this->identities = $identities;

    $this->random = new Random();
    $this->account = $this->client->getAccount();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): SqrlActionPluginBase {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('sqrl.handler'),
      $container->get('sqrl.client'),
      $container->get('sqrl.state'),
      $container->get('sqrl.log'),
      $container->get('config.factory'),
      $container->get('datetime.time'),
      $container->get('sqrl.identities')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function requiresSignatureRevalidation(): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function rememberSuccess(): SqrlActionInterface {
    $public_nut = $this->client->getNut()->getPublicNut();
    if ($this->account) {
      $this->state->setAuth($public_nut, $this->account->id());
    }
    elseif ($this->client->getIdentity()) {
      $this->state->setAuth($public_nut, $this->client->getIdentity()->getUsers());
    }
    return $this;
  }

}
