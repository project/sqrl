<?php

namespace Drupal\sqrl;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides assets services.
 */
class Assets {
  use StringTranslationTrait;

  /**
   * Gets an operation title.
   *
   * @param string $op
   *   The operation.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The title.
   */
  public function getOperationTitle(string $op): TranslatableMarkup {
    return match ($op) {
      'login' => $this->t('Login with SQRL'),
      'register' => $this->t('Register with SQRL'),
      'link' => $this->t('Link your SQRL identity'),
      'unlink' => $this->t('Unlink your SQRL identity'),
      'profile' => $this->t('Edit your password and email address'),
      default => $this->t('SQRL'),
    };
  }

  /**
   * Gets an operation descriptions.
   *
   * @param string $op
   *   The operation.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The description.
   */
  public function getOperationDescription(string $op): TranslatableMarkup {
    return match ($op) {
      'login' => $this->t('Use your SQRL client with this QR code in order to login to the site.'),
      'register' => $this->t('Use your SQRL client with this QR code in order to register a new account the site.'),
      'link' => $this->t('Use your SQRL client with this QR code in order to link your SQRL identity with the current user account.'),
      'unlink' => $this->t('Use your SQRL client with this QR code in order to unlink your SQRL identity from the current user account.'),
      'profile' => $this->t('Use your SQRL client with this QR code in order to edit your password and email address.'),
      default => $this->t('Use your SQRL client and capture the QR code.'),
    };
  }

  /**
   * Gets a description.
   *
   * @param string $type
   *   The type.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The description.
   */
  public function getOtherDescription(string $type): TranslatableMarkup {
    return match ($type) {
      'click' => $this->t('Click the logo to receive the SQRL widget.'),
      default => $this->t('SQRL'),
    };
  }

}
