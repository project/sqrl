<?php

namespace Drupal\sqrl;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\UserInterface;

/**
 * Provides services to alter login, register, password and user forms.
 */
class FormAlter {

  use StringTranslationTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected AccountProxy $currentUser;

  /**
   * The sqrl service.
   *
   * @var \Drupal\sqrl\Sqrl
   */
  protected Sqrl $sqrl;

  /**
   * The log channel.
   *
   * @var \Drupal\sqrl\Log
   */
  protected Log $log;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The identities.
   *
   * @var \Drupal\sqrl\Identities
   */
  protected Identities $identities;

  /**
   * Constructs a FormAlter object.
   *
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   The current user.
   * @param \Drupal\sqrl\Sqrl $sqrl
   *   The SQRL handler.
   * @param \Drupal\sqrl\Log $log
   *   The log channel.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\sqrl\Identities $identities
   *   The identities.
   */
  public function __construct(AccountProxy $current_user, Sqrl $sqrl, Log $log, EntityTypeManagerInterface $entity_type_manager, Identities $identities) {
    $this->currentUser = $current_user;
    $this->sqrl = $sqrl;
    $this->log = $log;
    $this->entityTypeManager = $entity_type_manager;
    $this->identities = $identities;
  }

  /**
   * Alters the login form.
   *
   * @param array $form
   *   The login form.
   */
  public function loginForm(array &$form): void {
    $this->addMarkup($form, 'login');
    $form['#validate'][] = [$this, 'loginFormValidate'];
  }

  /**
   * Alters the register form.
   *
   * @param array $form
   *   The register form.
   */
  public function registerForm(array &$form): void {
    if ($this->currentUser->isAnonymous()) {
      $this->addMarkup($form, 'register');
    }
  }

  /**
   * Alters the password form.
   *
   * @param array $form
   *   The password form.
   */
  public function passwordForm(array &$form): void {
    $form['#validate'][] = [$this, 'passwordFormValidate'];
  }

  /**
   * Alters the user form.
   *
   * @param array $form
   *   The user form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function userForm(array &$form, FormStateInterface $form_state): void {
    if (!isset($form_state->getStorage()['user'])) {
      // This is the registration form.
      return;
    }
    /** @var \Drupal\user\UserInterface $user */
    $user = $form_state->getStorage()['user'];
    if ($user->id() !== $this->currentUser->id() || !$this->identities->hasUserEnabledIdentities($user->id())) {
      return;
    }
    $form['account']['mail']['#access'] = FALSE;
    $form['account']['pass']['#access'] = FALSE;
    $form['account']['current_pass']['#access'] = FALSE;
    $form['sqrl'] = $this->sqrl->buildMarkup('profile');
    $form['sqrl']['#weight'] = -99;
  }

  /**
   * Helper function to add markup to a form.
   *
   * @param array $form
   *   The form.
   * @param string $op
   *   The operation.
   */
  private function addMarkup(array &$form, string $op): void {
    $form['sqrl'] = $this->sqrl->buildMarkup($op);
    $this->alterForm($form);
  }

  /**
   * Helper function to alter a form.
   *
   * @param array $form
   *   The form.
   */
  private function alterForm(array &$form): void {
    // Nothing to do at this point.
  }

  /**
   * Searches for a user account by values from a form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\user\UserInterface|null
   *   The user or NULL.
   */
  private function findAccountFromFormInput(FormStateInterface $form_state): ?UserInterface {
    try {
      $user_storage = $this->entityTypeManager->getStorage('user');
      $name = trim($form_state->getValue('name'));
      $users = $user_storage->loadByProperties(['mail' => $name]);
      if (empty($users)) {
        $users = $user_storage->loadByProperties(['name' => $name]);
      }
      /** @var \Drupal\user\UserInterface|bool $account */
      $account = reset($users);
      if ($account && $account->id()) {
        return $account;
      }
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException) {
    }
    return NULL;
  }

  /**
   * Validates the login form.
   *
   * @param array $form
   *   The login form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function loginFormValidate(array $form, FormStateInterface $form_state): void {
    if ($account = $this->findAccountFromFormInput($form_state)) {
      foreach ($this->identities->getIdentities($account->id()) as $identity) {
        if ($identity->isSqrlOnly()) {
          $form_state->setErrorByName('name', $this->t('%name can only login with SQRL.', ['%name' => $account->label()]));
          return;
        }
      }
    }
  }

  /**
   * Validates the password form.
   *
   * @param array $form
   *   The password form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function passwordFormValidate(array $form, FormStateInterface $form_state): void {
    if ($account = $this->findAccountFromFormInput($form_state)) {
      foreach ($this->identities->getIdentities($account->id()) as $identity) {
        if ($identity->isHardLocked()) {
          $form_state->setErrorByName('name', $this->t('%name is hard locked by SQRL client request.', ['%name' => $account->label()]));
          return;
        }
      }
    }
  }

}
