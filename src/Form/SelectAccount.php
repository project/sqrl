<?php

namespace Drupal\sqrl\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\sqrl\Nut;
use Drupal\sqrl\Sqrl;
use Drupal\sqrl\State;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a SQRL form.
 */
class SelectAccount extends FormBase {

  /**
   * The sqrl service.
   *
   * @var \Drupal\sqrl\Sqrl
   */
  protected Sqrl $sqrl;

  /**
   * The nut service.
   *
   * @var \Drupal\sqrl\Nut
   */
  protected Nut $nut;

  /**
   * The state.
   *
   * @var \Drupal\sqrl\State
   */
  protected State $state;

  /**
   * Link constructor.
   *
   * @param \Drupal\sqrl\Sqrl $sqrl
   *   The sqrl service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  final public function __construct(Sqrl $sqrl, MessengerInterface $messenger) {
    $this->sqrl = $sqrl;
    $this->messenger = $messenger;

    $this->nut = $this->sqrl->getNewNut();
    $this->nut->fetch();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): SelectAccount {
    return new SelectAccount(
      $container->get('sqrl.handler'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'sqrl_select_account';
  }

  /**
   * Checks the access permission.
   *
   * @param string $token
   *   The token.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(string $token): AccessResult {
    if ($this->nut->isValid() && $this->nut->getLoginToken() === $token) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['info'] = [
      '#markup' => $this->t('<p>You have been authenticated and your SQRL identity is associated to more than one account.</p>'),
    ];
    $form['account'] = [
      '#type' => 'radios',
      '#title' => $this->t('Select account'),
      '#options' => $this->nut->getAccountsForSelect(),
      '#required' => TRUE,
    ];
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Login'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->state->setAuth($this->nut->getPublicNut(), $form_state->getValue('account'));
    $this->sqrl->setNut($this->nut);
    $form_state->setRedirectUrl($this->sqrl->getUrl('sqrl.cps.url.login', ['token' => $this->nut->getLoginToken()]));
  }

}
