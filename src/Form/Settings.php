<?php

namespace Drupal\sqrl\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure SQRL settings for this site.
 */
class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'sqrl_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['sqrl.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('sqrl.settings');
    $form['account_name_pattern'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pattern for user names when creating them by SQRL'),
      '#description' => $this->t('Use the token [rand] for the random bytes, if missing they get appended.'),
      '#default_value' => $config->get('account_name_pattern'),
    ];
    $form['account_name_random_length'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Number of the random bytes'),
      '#size' => 6,
      '#default_value' => $config->get('account_name_random_length'),
    ];
    $form['qr_size'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Size of the QR code'),
      '#size' => 6,
      '#default_value' => $config->get('qr_size'),
    ];
    $form['poll_interval_initial'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Initial delay before polling starts, in seconds'),
      '#size' => 6,
      '#default_value' => $config->get('poll_interval_initial'),
    ];
    $form['poll_interval'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Delay between polling requests, in seconds'),
      '#size' => 6,
      '#default_value' => $config->get('poll_interval'),
    ];
    $form['allow_multiple_ids_per_account'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow multiple SQRL IDs per user account (NOT recommended).'),
      '#default_value' => $config->get('allow_multiple_ids_per_account'),
    ];
    $form['allow_multiple_accounts_per_id'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow multiple user accounts per SQRL ID (NOT recommended).'),
      '#default_value' => $config->get('allow_multiple_accounts_per_id'),
    ];
    $form['mail_verification'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Whether email address should be verified before storing to the database.'),
      '#default_value' => $config->get('mail_verification'),
    ];
    $form['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('In debug mode, JavaScript outputs to the browser console and the server writes to the Drupal logs.'),
      '#default_value' => $config->get('debug'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\Core\Config\Config $config */
    $config = $this->config('sqrl.settings');
    $config
      ->set('account_name_pattern', $form_state->getValue('account_name_pattern'))
      ->set('account_name_random_length', $form_state->getValue('account_name_random_length'))
      ->set('qr_size', $form_state->getValue('qr_size'))
      ->set('poll_interval_initial', $form_state->getValue('poll_interval_initial'))
      ->set('poll_interval', $form_state->getValue('poll_interval'))
      ->set('allow_multiple_ids_per_account', $form_state->getValue('allow_multiple_ids_per_account'))
      ->set('allow_multiple_accounts_per_id', $form_state->getValue('allow_multiple_accounts_per_id'))
      ->set('mail_verification', $form_state->getValue('mail_verification'))
      ->set('debug', $form_state->getValue('debug'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
