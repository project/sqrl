<?php

namespace Drupal\sqrl;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Interface for sqrl_action plugins.
 */
interface SqrlActionInterface extends ContainerFactoryPluginInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label(): string;

  /**
   * Determines if the signature needs to be revalidated.
   *
   * @return bool
   *   TRUE, if the signature needs to be revalidated, FALSE otherwise.
   */
  public function requiresSignatureRevalidation(): bool;

  /**
   * Perform the action.
   *
   * @return bool
   *   TRUE, if the action was successful, FALSE otherwise.
   *
   * @throws \Drupal\sqrl\Exception\ClientException
   */
  public function run(): bool;

  /**
   * Make sure the action result get remembered.
   *
   * @return self
   *   This action plugin.
   */
  public function rememberSuccess(): SqrlActionInterface;

}
