<?php

namespace Drupal\sqrl\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\sqrl\Assets;
use Drupal\sqrl\Sqrl;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the SQRL link controller.
 */
class View implements ContainerInjectionInterface {

  /**
   * The assets service.
   *
   * @var \Drupal\sqrl\Assets
   */
  protected Assets $assets;

  /**
   * The sqrl service.
   *
   * @var \Drupal\sqrl\Sqrl
   */
  protected Sqrl $sqrl;

  /**
   * Link constructor.
   *
   * @param \Drupal\sqrl\Assets $assets
   *   The assets service.
   * @param \Drupal\sqrl\Sqrl $sqrl
   *   The sqrl service.
   */
  final public function __construct(Assets $assets, Sqrl $sqrl) {
    $this->assets = $assets;
    $this->sqrl = $sqrl;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): View {
    return new View(
      $container->get('sqrl.assets'),
      $container->get('sqrl.handler')
    );
  }

  /**
   * Checks the access for this controller.
   *
   * @param string|null $op
   *   The operation.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(?string $op = NULL): AccessResult {
    // @todo check context like anonymous user etc.
    return AccessResult::allowedIf(in_array($op, ['register', 'login', 'link', 'unlink', 'profile']));
  }

  /**
   * Get the page title.
   *
   * @param string $op
   *   The operation.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The page title.
   */
  public function getTitle(string $op): TranslatableMarkup {
    return $this->assets->getOperationTitle($op);
  }

  /**
   * Handles the request.
   *
   * @param string $op
   *   The operation.
   *
   * @return array
   *   The render array.
   */
  public function request(string $op): array {
    return [
      'sqrl' => $this->sqrl->buildMarkup($op),
    ];
  }

}
