<?php

namespace Drupal\sqrl\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\sqrl\Response\QrCodeResponse;

/**
 * Provides the SQRL image controller.
 */
class Image extends Base {

  /**
   * Checks the access for the controller.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(): AccessResult {
    return AccessResult::allowedIf($this->client->getNut()->isValid());
  }

  /**
   * Handles the request.
   *
   * @return \Drupal\sqrl\Response\QrCodeResponse
   *   The response.
   */
  public function request(): QrCodeResponse {
    $response = new QrCodeResponse();
    $response->setQrCodeContent($this->sqrl->getNutUrl());
    return $response;
  }

}
