<?php

namespace Drupal\sqrl\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\sqrl\Identities;
use Drupal\sqrl\Sqrl;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the SQRL unlink controller.
 */
class Unlink implements ContainerInjectionInterface {

  /**
   * The sqrl service.
   *
   * @var \Drupal\sqrl\Sqrl
   */
  protected Sqrl $sqrl;

  /**
   * The identities.
   *
   * @var \Drupal\sqrl\Identities
   */
  protected Identities $identities;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * The configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Link constructor.
   *
   * @param \Drupal\sqrl\Sqrl $sqrl
   *   The sqrl service.
   * @param \Drupal\sqrl\Identities $identities
   *   The identities.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  final public function __construct(Sqrl $sqrl, Identities $identities, AccountProxyInterface $current_user, ConfigFactoryInterface $config_factory) {
    $this->sqrl = $sqrl;
    $this->identities = $identities;
    $this->currentUser = $current_user;
    $this->config = $config_factory->get('sqrl.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): Unlink {
    return new Unlink(
      $container->get('sqrl.handler'),
      $container->get('sqrl.identities'),
      $container->get('current_user'),
      $container->get('config.factory')
    );
  }

  /**
   * Checks the access for this controller.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(UserInterface $user): AccessResult {
    if ($this->currentUser->isAuthenticated() && $this->currentUser->id() === $user->id() && !empty($this->identities->getIdentities($user->id()))) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  /**
   * Handles the request.
   *
   * @return array
   *   The render array.
   */
  public function request(): array {
    return [
      'sqrl' => $this->sqrl->buildMarkup('unlink'),
    ];
  }

}
