<?php

namespace Drupal\sqrl\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\sqrl\Response\ClientResponse;

/**
 * Provides the SQRL client controller.
 */
class Client extends Base {

  /**
   * Checks access for this controller.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(): AccessResult {
    return AccessResult::allowedIf($this->client->getNut()->isValid());
  }

  /**
   * Handles the request.
   *
   * @return \Drupal\sqrl\Response\ClientResponse
   *   The response.
   */
  public function request(): ClientResponse {
    $content = $this->client->process();
    $this->log->debug('Client response: ' . $content);
    return new ClientResponse($content, 200, [
      'Content-Length' => strlen($content),
      'Content-Type' => 'application/x-www-form-urlencoded',
    ]);
  }

}
