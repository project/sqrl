<?php

namespace Drupal\sqrl\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\sqrl\Assets;
use Drupal\sqrl\Log;
use Drupal\sqrl\Nut;
use Drupal\sqrl\Sqrl;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides the SQRL CPS controller.
 */
class Cps implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The assets service.
   *
   * @var \Drupal\sqrl\Assets
   */
  protected Assets $assets;

  /**
   * The sqrl service.
   *
   * @var \Drupal\sqrl\Sqrl
   */
  protected Sqrl $sqrl;

  /**
   * The log channel.
   *
   * @var \Drupal\sqrl\Log
   */
  protected Log $log;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * The nut service.
   *
   * @var \Drupal\sqrl\Nut
   */
  private Nut $nut;

  /**
   * Link constructor.
   *
   * @param \Drupal\sqrl\Assets $assets
   *   The assets service.
   * @param \Drupal\sqrl\Sqrl $sqrl
   *   The sqrl service.
   * @param \Drupal\sqrl\Log $log
   *   The log channel.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  final public function __construct(Assets $assets, Sqrl $sqrl, Log $log, AccountProxyInterface $current_user, MessengerInterface $messenger) {
    $this->assets = $assets;
    $this->sqrl = $sqrl;
    $this->log = $log;
    $this->currentUser = $current_user;
    $this->messenger = $messenger;

    $this->nut = $this->sqrl->getNewNut();
    $this->nut->fetch();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): Cps {
    return new Cps(
      $container->get('sqrl.assets'),
      $container->get('sqrl.handler'),
      $container->get('sqrl.log'),
      $container->get('current_user'),
      $container->get('messenger')
    );
  }

  /**
   * Checks the access for the login controller.
   *
   * @param string $token
   *   The token.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result-
   */
  public function accessLogin(string $token): AccessResult {
    if ($this->nut->isValid() && $this->nut->getLoginToken() === $token) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  /**
   * Checks the access for the cancel controller.
   *
   * @param string $token
   *   The token.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result-
   */
  public function accessCancel(string $token): AccessResult {
    if ($this->nut->isValid() && $this->nut->getCancelToken() === $token) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  /**
   * Handles the login request.
   *
   * @param string $token
   *   The token.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The response.
   */
  public function login(string $token): RedirectResponse {
    if ($url = $this->nut->poll($token)) {
      return new RedirectResponse($url->toString());
    }
    return $this->cancel();
  }

  /**
   * Handles the cancel request.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The response.
   */
  public function cancel(): RedirectResponse {
    $this->messenger->addWarning($this->t('SQRL action canceled.'));
    return new RedirectResponse(Url::fromRoute('<front>')->toString());
  }

}
