<?php

namespace Drupal\sqrl\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\SettingsCommand;

/**
 * Provides the SQRL ajax controller.
 */
class Ajax extends Base {

  /**
   * Checks the access for this controller.
   *
   * @param string|null $op
   *   The operation.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(?string $op = NULL): AccessResult {
    if (!$this->client->getNut()->isValid()) {
      return AccessResult::forbidden();
    }
    return AccessResult::allowedIf(in_array($op, ['markup', 'poll']));
  }

  /**
   * Handles the request.
   *
   * @param string $op
   *   The operation.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The response.
   */
  public function request(string $op): AjaxResponse {
    $response = new AjaxResponse();
    switch ($op) {
      case 'markup':
        // @todo Return the SQRL markup and implement display in javascript.
        break;

      case 'poll':
        if ($url = $this->client->getNut()->poll()) {
          $response->addCommand(new SettingsCommand([
            'sqrl' => [
              'authenticated' => TRUE,
              'destination' => $url->toString(),
            ],
          ], TRUE));
        }
        break;

    }
    return $response;
  }

}
