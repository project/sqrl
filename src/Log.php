<?php

namespace Drupal\sqrl;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;

/**
 * Provides a log channel for sqrl.
 */
class Log {

  /**
   * The configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * Constructs a FormAlter object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger channel.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger) {
    $this->config = $config_factory->get('sqrl.settings');
    $this->logger = $logger->get('sqrl');
  }

  /**
   * Outputs a debug message.
   *
   * @param string $msg
   *   The message.
   * @param array $parameters
   *   The message parameters.
   */
  public function debug(string $msg, array $parameters = []): void {
    if ($this->config->get('debug')) {
      $this->logger->debug($msg, $parameters);
    }
  }

  /**
   * Outputs an error message.
   *
   * @param string $msg
   *   The message.
   * @param array $parameters
   *   The message parameters.
   */
  public function error(string $msg, array $parameters = []): void {
    $this->logger->error($msg, $parameters);
  }

}
