<?php

namespace Drupal\sqrl\Exception;

/**
 * Provides a Nut specific exception.
 */
class NutException extends \Exception {}
