<?php

namespace Drupal\sqrl\Exception;

/**
 * Provides a Client specific exception.
 */
class ClientException extends \Exception {}
