<?php

namespace Drupal\sqrl\Plugin\SqrlAction;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\sqrl\Exception\ClientException;
use Drupal\sqrl\SqrlActionPluginBase;

/**
 * Plugin implementation of the sqrl action "enable".
 *
 * @SqrlAction(
 *   id = "enable",
 *   label = @Translation("Enable"),
 *   description = @Translation("TBD.")
 * )
 */
class Enable extends SqrlActionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function requiresSignatureRevalidation(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function run(): bool {
    if ($this->client->getIdentity() === NULL) {
      return FALSE;
    }
    try {
      $this->client->getIdentity()
        ->setStatus(TRUE)
        ->save();
    }
    catch (EntityStorageException $e) {
      throw new ClientException('ID can not be updated');
    }
    return TRUE;
  }

}
