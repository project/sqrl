<?php

namespace Drupal\sqrl\Plugin\SqrlAction;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\sqrl\Exception\ClientException;
use Drupal\sqrl\SqrlActionPluginBase;

/**
 * Plugin implementation of the sqrl action "disable".
 *
 * @SqrlAction(
 *   id = "disable",
 *   label = @Translation("Disable"),
 *   description = @Translation("TBD.")
 * )
 */
class Disable extends SqrlActionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function run(): bool {
    if ($this->client->getIdentity() === NULL) {
      return FALSE;
    }
    try {
      $this->client->getIdentity()
        ->setStatus(FALSE)
        ->save();
    }
    catch (EntityStorageException $e) {
      throw new ClientException('ID can not be updated');
    }
    return FALSE;
  }

}
