<?php

namespace Drupal\sqrl\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\UserInterface;

/**
 * Provides an interface defining a sqrl entity type.
 */
interface IdentityInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the sqrl creation timestamp.
   *
   * @return int
   *   Creation timestamp of the sqrl.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the sqrl creation timestamp.
   *
   * @param int $timestamp
   *   The sqrl creation timestamp.
   *
   * @return \Drupal\sqrl\Entity\IdentityInterface
   *   The called sqrl entity.
   */
  public function setCreatedTime(int $timestamp): IdentityInterface;

  /**
   * Returns the sqrl status.
   *
   * @return bool
   *   TRUE if the sqrl is enabled, FALSE otherwise.
   */
  public function isEnabled(): bool;

  /**
   * Sets the sqrl status.
   *
   * @param bool $status
   *   TRUE to enable this sqrl, FALSE to disable.
   *
   * @return \Drupal\sqrl\Entity\IdentityInterface
   *   The called sqrl entity.
   */
  public function setStatus(bool $status): IdentityInterface;

  /**
   * Determines if only authentication by sqrl is allowed.
   *
   * @return bool
   *   TRUE, if this identity only allows authentication with sqrl, FALSE
   *   otherwise.
   */
  public function isSqrlOnly(): bool;

  /**
   * Set the identity to only allow authentication with sqrl.
   *
   * @param bool $flag
   *   TRUE, if authentication is only allowed with sqrl, FALSE otherwise.
   *
   * @return self
   *   This identity.
   */
  public function setSqrlOnly(bool $flag): IdentityInterface;

  /**
   * Determines if this identity is locked.
   *
   * @return bool
   *   TRUE, if this identity is locked, FALSE otherwise.
   */
  public function isHardLocked(): bool;

  /**
   * Set this identity locked or unlocked.
   *
   * @param bool $flag
   *   TRUE, if the identity should be locked, FALSE otherwise.
   *
   * @return self
   *   This identity.
   */
  public function setHardLocked(bool $flag): IdentityInterface;

  /**
   * Sets a successor identity.
   *
   * @param \Drupal\sqrl\Entity\IdentityInterface $identity
   *   The successor identity.
   *
   * @return self
   *   This identity.
   */
  public function setSuccessor(IdentityInterface $identity): IdentityInterface;

  /**
   * Adds a user to an identity.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   *
   * @return self
   *   This identity.
   */
  public function addUser(UserInterface $user): IdentityInterface;

  /**
   * Removes a user from this identity.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   *
   * @return self|null
   *   This identity or NULL, if no user is left.
   */
  public function removeUser(UserInterface $user): ?IdentityInterface;

  /**
   * Determines if identity has a successor.
   *
   * @return bool
   *   TRUE, if identity has a successor, FALSE otherwise.
   */
  public function hasSuccessor(): bool;

  /**
   * Gets the first user of this identity.
   *
   * @return \Drupal\user\UserInterface
   *   The user.
   */
  public function getFirstUser(): UserInterface;

  /**
   * Get the user, if it is associated with this identity.
   *
   * @param int $uid
   *   The user ID.
   *
   * @return \Drupal\user\UserInterface|null
   *   The user, if it is associated with this identity, FALSE otherwise.
   */
  public function getUser(int $uid): ?UserInterface;

  /**
   * Get all users of this identity.
   *
   * @return \Drupal\user\UserInterface[]
   *   The users.
   */
  public function getUsers(): array;

  /**
   * Get the SUK of this identity.
   *
   * @return string
   *   The SUK.
   */
  public function getSuk(): string;

  /**
   * Get the VUK of this identity.
   *
   * @return string
   *   The VUK.
   */
  public function getVuk(): string;

  /**
   * Delete this identity and its successors.
   *
   * @return bool
   *   TRUE, if deletion succeeded, FALSE otherwise.
   */
  public function deleteAll(): bool;

}
