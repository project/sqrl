<?php

namespace Drupal\sqrl\Entity;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\user\UserInterface;

/**
 * Defines the sqrl identity entity class.
 *
 * @ContentEntityType(
 *   id = "sqrl_identity",
 *   label = @Translation("SQRL identity"),
 *   handlers = {},
 *   base_table = "sqrl_identity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid"
 *   },
 * )
 */
class Identity extends ContentEntityBase implements IdentityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status): IdentityInterface {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isSqrlOnly(): bool {
    return (bool) $this->get('sqrlonly')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSqrlOnly($flag): IdentityInterface {
    $this->set('sqrlonly', $flag);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isHardLocked(): bool {
    return (bool) $this->get('hardlock')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setHardLocked($flag): IdentityInterface {
    $this->set('hardlock', $flag);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setSuccessor(IdentityInterface $identity): IdentityInterface {
    $this->set('sid', $identity);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp): IdentityInterface {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasSuccessor(): bool {
    return ($this->get('sid')->entity !== NULL);
  }

  /**
   * {@inheritdoc}
   */
  public function addUser($user): IdentityInterface {
    $users = $this->getUsers();
    $users[] = $user;
    $this->set('user', $users);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removeUser($user): ?IdentityInterface {
    $users = $this->getUsers();
    foreach ($users as $key => $existing_user) {
      if ($user->id() === $existing_user->id()) {
        unset($users[$key]);
        break;
      }
    }
    if (empty($users)) {
      $this->deleteAll();
      return NULL;
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFirstUser(): UserInterface {
    return $this->getUsers()[0];
  }

  /**
   * {@inheritdoc}
   */
  public function getUser($uid): ?UserInterface {
    $users = $this->getUsers();
    foreach ($users as $user) {
      if ($uid === $user->id()) {
        return $user;
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getUsers(): array {
    return $this->get('user')->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function getSuk(): string {
    return $this->get('suk')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getVuk(): string {
    return $this->get('vuk')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAll(): bool {
    if ($this->hasSuccessor()) {
      /** @var \Drupal\sqrl\Entity\IdentityInterface $successor */
      $successor = $this->get('sid')->entity;
      return $successor->deleteAll();
    }

    $id = $this->id();
    try {
      $this->delete();
      while ($next = $this->entityTypeManager()->getStorage('sqrl_entity')->loadByProperties(['sid' => $id])) {
        /** @var \Drupal\sqrl\Entity\IdentityInterface $next */
        $id = $next->id();
        $next->delete();
      }
      return TRUE;
    }
    catch (EntityStorageException | InvalidPluginDefinitionException | PluginNotFoundException) {
      // @todo Log this exception.
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDescription(t('A boolean indicating whether the sqrl identity is enabled.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled');

    $fields['sqrlonly'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('SQRL only'))
      ->setDescription(t('A boolean indicating whether other authentication methods should also be accepted.'))
      ->setDefaultValue(FALSE)
      ->setSetting('on_label', 'Enabled');

    $fields['hardlock'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Hardlock'))
      ->setDescription(t('A boolean indicating whether account recovery should be disabled.'))
      ->setDefaultValue(FALSE)
      ->setSetting('on_label', 'Enabled');

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created on'))
      ->setDescription(t('The time that the sqrl identity was created.'))
      ->setRequired(TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the sqrl identity was last edited.'))
      ->setRequired(TRUE);

    $fields['user'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User'))
      ->setDescription(t('The user ID of this identity.'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setRequired(TRUE);

    $fields['idk'] = BaseFieldDefinition::create('string')
      ->setLabel(t('IDK'))
      ->setDescription(t('The IDK of the identity.'))
      ->setReadOnly(TRUE)
      ->setRequired(TRUE)
      ->setSettings([
        'max_length' => 64,
        'text_processing' => 0,
      ]);
    $fields['suk'] = BaseFieldDefinition::create('string')
      ->setLabel(t('SUK'))
      ->setDescription(t('The SUK of the identity.'))
      ->setSettings([
        'max_length' => 64,
        'text_processing' => 0,
      ])
      ->setRequired(TRUE);
    $fields['vuk'] = BaseFieldDefinition::create('string')
      ->setLabel(t('VUK'))
      ->setDescription(t('The VUK of the identity.'))
      ->setSettings([
        'max_length' => 64,
        'text_processing' => 0,
      ])
      ->setRequired(TRUE);
    $fields['sid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Successor'))
      ->setDescription(t('The new identity superseding this one.'))
      ->setSetting('target_type', 'sqrl_identity')
      ->setSetting('handler', 'default');

    return $fields;
  }

}
