<?php

namespace Drupal\sqrl;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\KeyValueStore\KeyValueExpirableFactory;
use Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides state services.
 */
final class State extends KeyValueExpirableFactory {

  public const EXPIRE_AUTH = 120;
  public const EXPIRE_NUT = 600;

  /**
   * Key value store for authentication.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  protected KeyValueStoreExpirableInterface $auth;

  /**
   * Key value store for nuts.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  protected KeyValueStoreExpirableInterface $nut;

  /**
   * Key value store for messages.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  protected KeyValueStoreExpirableInterface $messages;

  /**
   * {@inheritdoc}
   */
  public function __construct(ContainerInterface $container, array $options = []) {
    parent::__construct($container, $options);
    $this->auth = $this->get('sqrl_auth');
    $this->nut = $this->get('sqrl_nut');
    $this->messages = $this->get('sqrl_messages');
  }

  /**
   * Sets the authentication value.
   *
   * @param string $nut
   *   The nut.
   * @param int|\Drupal\user\UserInterface[] $users
   *   The list of users or a user ID.
   *
   * @return self
   *   This state.
   *
   * @throws \JsonException
   */
  public function setAuth(string $nut, int|array $users): State {
    if (is_array($users)) {
      $uids = [];
      foreach ($users as $user) {
        $uids[] = $user->id();
      }
    }
    else {
      $uids = [$users];
    }
    $this->auth->setWithExpire($nut, json_encode($uids, JSON_THROW_ON_ERROR), self::EXPIRE_AUTH);
    return $this;
  }

  /**
   * Sets the nut value.
   *
   * @param string $nut
   *   The nut.
   * @param array $values
   *   The nut values.
   *
   * @return self
   *   This state.
   *
   * @throws \JsonException
   */
  public function setNut(string $nut, array $values): State {
    $this->nut->setWithExpire($nut, json_encode($values, JSON_THROW_ON_ERROR), self::EXPIRE_NUT);
    return $this;
  }

  /**
   * Get the authentication values.
   *
   * @param string $nut
   *   The nut.
   * @param bool $delete
   *   Whether the values should be deleted from state.
   *
   * @return int[]
   *   The values.
   *
   * @throws \JsonException
   */
  public function getAuth(string $nut, bool $delete = TRUE): array {
    if ($data = $this->auth->get($nut)) {
      if ($delete) {
        $this->auth->delete($nut);
      }
      return json_decode($data, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    return [];
  }

  /**
   * Get the nut values.
   *
   * @param string $nut
   *   The nut.
   *
   * @return array
   *   The values.
   *
   * @throws \JsonException
   */
  public function getNut(string $nut): array {
    if ($data = $this->nut->get($nut)) {
      return json_decode($data, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    return [];
  }

  /**
   * Add a message.
   *
   * @param string $nut
   *   The nut.
   * @param \Drupal\Component\Render\MarkupInterface|string $message
   *   The message.
   * @param string $type
   *   The message type.
   *
   * @return self
   *   This state.
   *
   * @throws \JsonException
   */
  public function addMessage(string $nut, MarkupInterface|string $message, string $type = MessengerInterface::TYPE_STATUS): State {
    $messages = $this->getMessages($nut);
    $messages[] = [
      'message' => $message,
      'type' => $type,
    ];
    $this->messages->setWithExpire($nut, json_encode($messages, JSON_THROW_ON_ERROR), self::EXPIRE_NUT);
    return $this;
  }

  /**
   * Get the messages.
   *
   * @param string $nut
   *   The nut.
   *
   * @return array
   *   The messages.
   *
   * @throws \JsonException
   */
  public function getMessages(string $nut): array {
    if ($data = $this->messages->get($nut)) {
      return json_decode($data, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function get($collection): KeyValueStoreExpirableInterface {
    /** @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface $store */
    $store = parent::get($collection);
    return $store;
  }

}
