<?php

namespace Drupal\sqrl\Response;

use Endroid\QrCode\QrCode;
use Endroid\QrCode\Writer\PngWriter;
use Symfony\Component\HttpFoundation\Response;

/**
 * Response which is returned as the QR code image.
 */
class QrCodeResponse extends Response {

  /**
   * The qr code.
   *
   * @var \Endroid\QrCode\QrCode|null
   */
  protected ?QrCode $qrCode = NULL;

  /**
   * Sets the content of the qr code.
   *
   * @param string $content
   *   The content.
   */
  public function setQrCodeContent(string $content): void {
    $this->qrCode = new QrCode($content);
  }

  /**
   * {@inheritdoc}
   */
  public function sendHeaders(?int $statusCode = NULL): static {
    $this->headers->set('content-type', 'image/png');
    return parent::sendHeaders($statusCode);
  }

  /**
   * {@inheritdoc}
   */
  public function sendContent(): static {
    if ($this->qrCode === NULL) {
      return $this;
    }

    // Begin capturing the byte stream.
    ob_start();
    $writer = new PngWriter();
    print $writer->write($this->qrCode)->getString();
    return $this;
  }

}
