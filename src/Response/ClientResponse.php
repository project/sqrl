<?php

namespace Drupal\sqrl\Response;

use Symfony\Component\HttpFoundation\Response;

/**
 * Response which return plain text content to the SQRL client.
 */
class ClientResponse extends Response {

  /**
   * {@inheritdoc}
   */
  public function sendHeaders(?int $statusCode = NULL): static {
    $this->headers->set('Content-Type', 'application/x-www-form-urlencoded');
    $this->headers->set('Content-Length', (string) strlen($this->content));
    return parent::sendHeaders($statusCode);
  }

}
