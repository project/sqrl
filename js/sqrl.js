(function ($, Drupal, drupalSettings) {
  Drupal.sqrl = Drupal.sqrl || {};
  drupalSettings.sqrl = drupalSettings.sqrl || {};
  drupalSettings.sqrl.pollIntervalInitial =
    drupalSettings.sqrl.pollIntervalInitial || 5;
  drupalSettings.sqrl.pollInterval = drupalSettings.sqrl.pollInterval || 2;
  drupalSettings.sqrl.authenticated =
    drupalSettings.sqrl.authenticated || false;
  drupalSettings.sqrl.canceled = drupalSettings.sqrl.canceled || false;
  drupalSettings.sqrl.debug = drupalSettings.sqrl.debug || false;

  Drupal.behaviors.sqrl = {
    attach() {
      $('#sqrl-cache:not(.sqrl-processed)')
        .addClass('sqrl-processed')
        .on('click', function () {
          Drupal.ajax({
            url: drupalSettings.sqrl.url.markup,
            error(e) {
              Drupal.sqrl.debug(e.toString());
              drupalSettings.sqrl.canceled = true;
            },
          }).execute();
          Drupal.sqrl.poll(drupalSettings.sqrl.pollIntervalInitial);
        });
      $('.sqrl:not(.sqrl-processed)')
        .addClass('sqrl-processed')
        .each(function () {
          Drupal.sqrl.poll(drupalSettings.sqrl.pollIntervalInitial);
        });
      $('.sqrl a.cps:not(.sqrl-processed)')
        .addClass('sqrl-processed')
        .on('click', function () {
          const encodedSqrlUrl = $(this).attr('encoded-sqrl-url');
          if (encodedSqrlUrl) {
            drupalSettings.sqrl.canceled = true;
            const gifProbe = new Image();
            const localhostRoot = 'http://localhost:25519/';
            Date.now =
              Date.now ||
              function () {
                return +new Date();
              };
            gifProbe.onload = function () {
              window.location = localhostRoot + encodedSqrlUrl;
            };
            gifProbe.onerror = function () {
              setTimeout(function () {
                gifProbe.src = `${localhostRoot + Date.now()}.gif`;
              }, 250);
            };
            gifProbe.onerror();
          }
          return true;
        });
    },
  };

  Drupal.sqrl.poll = function (timeout) {
    setTimeout(function () {
      if (drupalSettings.sqrl.canceled) {
        // Nothing to do, we just have to cancel polling.
      } else if (!drupalSettings.sqrl.authenticated) {
        Drupal.sqrl.debug('poll');
        Drupal.ajax({
          url: drupalSettings.sqrl.url.poll,
          error(e) {
            Drupal.sqrl.debug(e.toString());
            drupalSettings.sqrl.canceled = true;
          },
        }).execute();
        Drupal.sqrl.poll(drupalSettings.sqrl.pollInterval);
      } else if (drupalSettings.sqrl.destination) {
        Drupal.sqrl.debug('done !!!');
        window.location = drupalSettings.sqrl.destination;
      }
    }, timeout);
  };

  Drupal.sqrl.debug = function (text) {
    if (drupalSettings.sqrl.debug) {
      console.log(text);
    }
  };
})(jQuery, Drupal, drupalSettings);
